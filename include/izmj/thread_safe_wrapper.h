﻿#pragma once

/**
 * @file izmj_ts_wrapper.h
 * @author GiraKoo (girakoo@163.com)
 * @brief 线程安全(Thread Safe)的数据Wrapper
 * @version 0.1
 * @date 2024-03-18
 * 
 */

#include <string>
#include <mutex>

// - 本类的目的是将线程不安全的类型指针，封装在内部。
// - 强制要求使用者需要通过lock方法，获得数据。使用结束后，利用unlock方法解锁。
// - 当前内部使用mutex进行加锁处理。可以根据实际使用环境，决定使用Mutex还是RWLock,SpinLock替换实现。
// - Wrapper本身不参与空间的创建与释放。

template <typename T>
class TsWrapper
{
public:
    TsWrapper() = default;
    TsWrapper(const T* value) : m_value(value) {}
    virtual ~TsWrapper() = default;

    /**
     * @brief 设置数据
     * 
     * @param value 数据
     */
    void set(T* value) {
        std::lock_guard<std::mutex> _guard(m_mutex);
        m_value = value;
    }

    /**
     * @brief 获取Wrapper中的数据，使用完毕后，需要unlock
     * 
     * @return T* 数据
     */
    T* lock() {
        m_mutex.lock();
        return m_value;
    }

    /**
     * @brief 解锁数据
     * 
     */
    void unlock() {
        m_mutex.unlock();
    }

private:
    T* m_value = nullptr;
    std::mutex m_mutex;
};