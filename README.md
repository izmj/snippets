# 代码片段 Snippets

本库用于保存，测试一些通用的工具类。使用gtest进行测试。

## 目录 Directory

用于记录当前Snippets中包含的那些类。

### 线程安全类 Thread-safe

用于实现线程安全的类

- thread_safe_wrapper.h: 线程安全的包装类。将需要被保护的类放到模板中。使用前，需要通过lock获取真实对象。使用结束后，需要使用unlock解锁。
  
## 第三方库 Third-party

- gtest: 单体测试工具
