#include "izmj/thread_safe_wrapper.h"

#include <thread>

#if defined(__linux__)
    #define PLATFORM_LINUX 1
#elif defined(_WIN32) || defined(WIN32)
    #define PLATFORM_WIN32 1
#endif

#if defined(PLATFORM_LINUX)
    #include <unistd.h>
#elif defined(PLATFORM_WIN32)
    #include <windows.h>
#endif

#include <gtest/gtest.h>

TsWrapper<int> g_tsWrapperData;

TEST(TsWrapperTest, MultiThreadTest)
{
    auto data = new int(0);
    g_tsWrapperData.set(data);

    const int thread1Count = 100;
    const int thread2Count = 500;

    std::thread t1([&thread1Count]() {
        for (int i = 0; i < thread1Count; i++) {
            auto data = g_tsWrapperData.lock();
            *data = *data + 1;
            g_tsWrapperData.unlock();
#ifdef PLATFORM_LINUX
            usleep(1);
#else
            Sleep(1);
#endif
        }
    });
    

    std::thread t2([&thread2Count]() {
        for (int i = 0; i < thread2Count; i++) {
            auto data = g_tsWrapperData.lock();
            *data = *data + 1;
            g_tsWrapperData.unlock();
#ifdef PLATFORM_LINUX
            usleep(1);
#else
            Sleep(1);
#endif
        }
    });

    t1.join();
    t2.join();

    ASSERT_EQ(thread1Count + thread2Count, *data);

    delete data;
}

int main(int argc, char* argv[]) 
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}